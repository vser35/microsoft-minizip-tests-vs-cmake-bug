﻿// minizip-tests.cpp : Defines the entry point for the application.
//

#include "minizip-tests.h"



#include <fstream>
#include <vector>
//#include <minizip/unzip.h>
#include <minizip/zip.h>

// https://stackoverflow.com/questions/11370908/how-do-i-use-minizip-on-zlib/25743880#25743880
int CreateZipFile(std::vector<std::string> src_paths, std::string dest_path)
{
    zipFile zf = zipOpen(dest_path.c_str(), APPEND_STATUS_CREATE);
    if (zf == NULL)
        return 1;

    bool _return = true;
    for (size_t i = 0; i < src_paths.size(); i++) {
        std::fstream file(src_paths[i].c_str(), std::ios::binary | std::ios::in);
        if (file.is_open()) {
            file.seekg(0, std::ios::end);
            long size = file.tellg();
            file.seekg(0, std::ios::beg);

            std::vector<char> buffer(size);
            if (size == 0 || file.read(&buffer[0], size)) {
                zip_fileinfo zfi = { 0 };
                std::string fileName = src_paths[i].substr(src_paths[i].rfind('\\') + 1);

                if (0 == zipOpenNewFileInZip(zf, std::string(fileName.begin(), fileName.end()).c_str(), &zfi, NULL, 0, NULL, 0, NULL, Z_DEFLATED, Z_DEFAULT_COMPRESSION)) {
                    if (zipWriteInFileInZip(zf, size == 0 ? "" : &buffer[0], size))
                        _return = false;

                    if (zipCloseFileInZip(zf))
                        _return = false;

                    file.close();
                    continue;
                }
            }
            file.close();
        }
        _return = false;
    }

    if (zipClose(zf, NULL))
        return 3;

    if (!_return)
        return 4;
    return 0;
}

int main()
{
	std::cout << "Hello CMake.\n";
	return 0;
}

